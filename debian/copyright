Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wifipumpkin3
Source: https://github.com/P0cL4bs/wifipumpkin3

Files: *
Copyright: 2020-2022 P0cL4bs Team - Marcos Bomfim (mh4x0f)
License: Apache-2.0

Files: wifipumpkin3/plugins/bin/sslstrip3.py
Copyright: 2004-2009 Moxie Marlinspike <moxie@thoughtcrime.org>
License: GPL-3+

Files: config/templates/loginPage/static/css/bootstrap.min.css
 config/templates/Login_v4/static/vendor/bootstrap/css/bootstrap.css
 config/templates/Login_v4/static/vendor/bootstrap/css/bootstrap.css.map
 config/templates/Login_v4/static/vendor/bootstrap/css/bootstrap.min.css
 config/templates/Login_v4/static/vendor/bootstrap/css/bootstrap.min.css.map
Copyright: 2011-2018 Twitter, Inc. / 2011-2018 The Bootstrap Authors
License: MIT

Files: config/templates/loginPage/static/js/bootstrap.min.js
 config/templates/Login_v4/static/vendor/bootstrap/js/bootstrap.js
 config/templates/Login_v4/static/vendor/bootstrap/js/bootstrap.min.js
Copyright: 2011-2018 The Bootstrap Authors
License: MIT

Files: config/templates/loginPage/static/js/jquery-1.11.1.min.js
 config/templates/Login_v4/static/vendor/jquery/jquery-3.2.1.min.js
Copyright: 2005, 2014 jQuery Foundation, Inc.
License: MIT

Files: config/templates/Login_v4/static/vendor/daterangepicker/daterangepicker.js
Copyright: 2012-2017 Dan Grossman.
License: MIT

Files: config/templates/Login_v4/static/vendor/select2/select2.js
Copyright: 2011-2014 The Dojo Foundation
License: MIT

Files: config/templates/Login_v4/static/vendor/bootstrap/js/popper.js
 config/templates/Login_v4/static/vendor/bootstrap/js/popper.min.js
 config/templates/Login_v4/static/vendor/bootstrap/js/tooltip.js
Copyright: 2016-2017 Federico Zivolo and contributors
License: MIT

Files: debian/*
Copyright: 2022 Sophie Brun <sophie@offensive-security.com>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License. You may obtain a
 copy of the License at
 .
   http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations
 under the License.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to permit
 persons to whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
